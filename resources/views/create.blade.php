<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Employee</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
@endif
      <h2>Add Employee</h2><br/>
      <form method="post" action="{{url('employee/create')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
          <div class="form-group col-md-4">
            <input type="text" class="form-control" name="fname" placeholder="Full Name" required>
          </div>
          
          <div class="form-group col-md-4">
            <input type="text" class="form-control" name="username" placeholder="Username" required>
          </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
              <input type="text" class="form-control" name="mname" placeholder="Middle Name" required>
            </div>
            
          <div class="form-group col-md-4">
            <input type="password" class="form-control" name="password" placeholder="Password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
          </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
              <input type="text" class="form-control" name="lname" placeholder="Last Name" required>
            </div>
            
          <div class="form-group col-md-4">
            <input type="password" class="form-control" name="cpassword" placeholder="Confirm Password" required>
          </div>
        </div>
        
        <div class="row">
            <div class="form-group col-md-4">
              <input type="email" class="form-control" name="email" placeholder="E-mail" required>
            </div>
            <div class="form-group col-md-4">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
          
        <div class="row">
          <div class="form-group col-md-4">
            <input type="number" class="form-control" name="telp" placeholder="Telp">
          </div>
          
        </div>

        <div class="row">
          <div class="form-group col-md-4">
            <input type="date" class="form-control" name="dob">
          </div>
        </div>
         
      </form>
    </div>
  </body>
</html>