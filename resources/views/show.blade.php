<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Company</title>
    <style>
        .martop-sm {margin-top: 15px;}
        .martop-lg {margin-top: 70px;}
    </style>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container martop-lg">
        <div class="panel panel-default">
            <div class="panel-body">
                        
                <h1><b><i>{{$company->name}}</i></h1>
                <h2>Address : {{$company->address}}</h2>
                <h2>Phone : {{$company->phone}}</h2>
                <h2>E-mail : {{$company->email}}</h2>
                
                    
            </div>
        </div>

        <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Full Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
        <th>E-Mail</th>
        <th>Telp</th>
        <th>Date of Birth</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>
      
      @foreach($employees as $employee)
      <tr>
        <td>{{$employee['id']}}</td>
        <td>{{$employee['f_name']}}</td>
        <td>{{$employee['m_name']}}</td>
        <td>{{$employee['l_name']}}</td>
        <td>{{$employee['email']}}</td>
        <td>{{$employee['telp']}}</td>
        <td>{{$employee['dob']}}</td>
        <td><a href="{{action('EmployeeController@edit', $employee['id'])}}" class="btn btn-warning">Edit</a></td>
        <td>
          <form action="{{action('EmployeeController@destroy', $employee['id'])}}" method="post">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

        

        
    </div>
  </body>
</html>