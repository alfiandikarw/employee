<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Employee</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Update Karyawan</h2>
       @if ($errors->any())       
       <div class="alert alert-danger">           
           <ul>              
                @foreach ($errors->all() as $error)                   
                <li>{{ $error }}</li>               
                @endforeach           
            </ul>       
        </div><br /> 
        @endif
      <form action="{{ action('EmployeeController@update',$id)}}" method="POST">
      {{ csrf_field() }}
      <input name="_method" type="hidden" value="PATCH">
        <div class="row">
          <div class="form-group col-md-4">
            <input type="text" class="form-control" name="fname" value="{{$employee->f_name}}">
          </div>
        </div>

        <div class="row">
            <div class="form-group col-md-4">
              <input type="text" class="form-control" name="mname" value="{{$employee->m_name}}">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-4">
              <input type="text" class="form-control" name="lname" value="{{$employee->l_name}}">
            </div>
        </div>

          
        <div class="row">
          <div class="form-group col-md-4">
            <input type="email" class="form-control" name="email" value="{{$employee->email}}">
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-4">
            <input type="number" class="form-control" name="telp" value="{{$employee->telp}}">
          </div>
        </div>

        
        <div class="row">
          <div class="form-group col-md-4">
            <input type="date" class="form-control" name="dob" value="{{$employee->dob}}">
          </div>
        </div>
         
        <div class="row">
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-warning">Update</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>