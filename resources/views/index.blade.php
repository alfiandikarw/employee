<!-- index.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Employee</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
     <div class="row">
          <div class="col-md-10">
            <h2>Data Employee</h2>
          </div>
          <div class="col-md-2"><a href="{{url('employee/create')}}">
            <button type="submit" class="btn btn-primary" style="margin-top: 20px">Add Employee</button></a>
          </div>
        </div>
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Full Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
        <th>E-Mail</th>
        <th>Telp</th>
        <th>Date of Birth</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>
      
      @foreach($employees as $employee)
      <tr>
        <td>{{$employee['id']}}</td>
        <td>{{$employee['f_name']}}</td>
        <td>{{$employee['m_name']}}</td>
        <td>{{$employee['l_name']}}</td>
        <td>{{$employee['email']}}</td>
        <td>{{$employee['telp']}}</td>
        <td>{{$employee['dob']}}</td>
        <td><a href="{{action('EmployeeController@edit', $employee['id'])}}" class="btn btn-warning">Edit</a></td>
        <td>
          <form action="{{action('EmployeeController@destroy', $employee['id'])}}" method="post">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  </body>
</html>