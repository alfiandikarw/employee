@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <!-- index.blade.php -->
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
     <div class="row">
          <div class="col-md-10">
            <h2>Data Company</h2>
          </div>
          <div class="col-md-2"><a href="{{url('company/create')}}">
            <button type="submit" class="btn btn-primary" style="margin-top: 20px">Add Company</button></a>
          </div>
        </div>
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Address</th>
        <th>Phone</th>
        <th>E-mail</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>
      
      @foreach($companies as $company)
      <tr>
        <td>{{$company['id']}}</td>
        <td>{{$company['name']}}</td>
        <td>{{$company['address']}}</td>
        <td>{{$company['phone']}}</td>
        <td>{{$company['email']}}</td>
        <td><a href="{{action('CompanyController@show', $company['id'])}}" class="btn btn-info">Detail</a>
        <a href="{{action('CompanyController@edit', $company['id'])}}" class="btn btn-warning">Edit</a></td>
        <td>
          <form action="{{action('CompanyController@destroy', $company['id'])}}" method="post">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  
  </div>


    </div>
</div>
@endsection
