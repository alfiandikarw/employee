<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Company</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
@endif
      <h2>Add Company</h2><br/>
      <form method="post" action="{{url('company/create')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
          <div class="form-group col-md-4">
            <input type="text" class="form-control" name="name" placeholder="Name Company">
          </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
              <input type="text" class="form-control" name="address" placeholder="Address">
            </div>
        </div>
        
        <div class="row">
          <div class="form-group col-md-4">
            <input type="number" class="form-control" name="phone" placeholder="Phone">
          </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
              <input type="email" class="form-control" name="email" placeholder="E-mail">
            </div>
        </div>
          
          
        <div class="row">
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>