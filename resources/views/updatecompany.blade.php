<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Company</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Update Company</h2>
       @if ($errors->any())       
       <div class="alert alert-danger">           
           <ul>              
                @foreach ($errors->all() as $error)                   
                <li>{{ $error }}</li>               
                @endforeach           
            </ul>       
        </div><br /> 
        @endif
      <form action="{{ action('CompanyController@update',$id)}}" method="POST">
      {{ csrf_field() }}
      <input name="_method" type="hidden" value="PATCH">
        <div class="row">
          <div class="form-group col-md-4">
            <input type="text" class="form-control" name="name" value="{{$company->name}}">
          </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
              <input type="text" class="form-control" name="address" value="{{$company->address}}">
            </div>
        </div>
          
        <div class="row">
          <div class="form-group col-md-4">
            <input type="number" class="form-control" name="phone" value="{{$company->phone}}">
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-4">
            <input type="email" class="form-control" name="email" value="{{$company->email}}">
          </div>
        </div>

        
         
        <div class="row">
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-warning">Update</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>