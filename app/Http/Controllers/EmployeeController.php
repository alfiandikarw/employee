<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $employees = \App\Employee::all();
        return view('index', compact('employees'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //menyimpan data ke database

        $employee = new \App\Employee;
        $employee->f_name = $request->get('fname');
        $employee->l_name = $request->get('lname');
        $employee->m_name = $request->get('mname');
        $employee->email = $request->get('email');
        $employee->telp = $request->get('telp');
        $employee->dob = $request->get('dob');
        $employee->save();

        return redirect('employees')->with('Success','Data has been created!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee,$id)
    {
        //
        $employee = \App\Employee::find($id);
        return $employee;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $employee = \App\Employee::find($id);
        return view('update', compact('employee','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Menyimpan data yang diubah

        $employee = \App\Employee::find($id);
        $employee->fname = $request->get('fname');
        $employee->m_name = $request->get('mname');
        $employee->l_name = $request->get('lname');
        $employee->email = $request->get('email');
        $employee->telp = $request->get('telp');
        $employee->dob = $request->get('dob');
        $employee->save();

        return redirect('employees')->with('Success','Data has been update!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //menghapus data

        $employee = \App\Employee::find($id);
        $employee->delete();

        return redirect('employees')->with('Success','Data has been deleted!');
    }

}
