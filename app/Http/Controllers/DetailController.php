<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;


class DetailController extends Controller
{
    //

    public function index($id){

        $company = \App\Company::all();
        $employees = \App\Employee::all();

        return view('show', compact('company','employees'));
    }
}
