<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use App\Employee;

class CompanyController extends Controller
{
    //

    public function index(){

        $companies = \App\Company::all();
        return view('home', compact('companies'));


    }

    public function create(){
        return view('createcompany');
    }

    public function store(request $request){

        $company = new \App\Company;
        $company->name = $request->get('name');
        $company->address = $request->get('address');
        $company->phone = $request->get('phone');
        $company->email = $request->get('email');
        $company->save();

        return redirect('companies')->with('Success', 'Data has been created!');

    }

    public function show(Company $company, $id)
    {
        //
        $company = \App\Company::find($id);
        $employees = \App\Employee::all();
        return view('show',compact('company','employees'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $company = \App\Company::find($id);
        return view('updatecompany', compact('company','id'));
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Menyimpan data yang diubah

        $company = \App\Company::find($id);
        $company->name = $request->get('name');
        $company->address = $request->get('address');
        $company->phone = $request->get('phone');
        $company->email = $request->get('email');
        $company->save();

        return redirect('companies')->with('Success', 'Data has been update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //menghapus data

        $company = \App\Company::find($id);
        $company->delete();

        return redirect('companies')->with('Success', 'Data has been deleted!');
    }

}
