<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/employee/create', 'EmployeeController@create');
Route::get('/employees','EmployeeController@index');
ROute::post('/employee/create','EmployeeController@store');
Route::get('/employee/edit/{id}','EmployeeController@edit')->name('employee.edit');
Route::patch('/employee/update/{id}', 'EmployeeController@update');
Route::delete('/employee/delete/{id}','EmployeeController@destroy');
Auth::routes();

Route::get('/home', 'CompanyController@index')->name('home');

Route::get('/company/create', 'CompanyController@create');
Route::get('/companies', 'CompanyController@index');
ROute::post('/company/create', 'CompanyController@store');
Route::get('/company/edit/{id}', 'CompanyController@edit');
Route::patch('/company/update/{id}', 'CompanyController@update');
Route::delete('/company/delete/{id}', 'CompanyController@destroy');
Route::get('/company/show/{id}', 'CompanyController@show');